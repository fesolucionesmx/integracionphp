<?php
	/**
	 * Permite timbrar una factura
	 */
	function timbrarFactura($complementonotario=false,$complementoestimaciones=false)
	{
		$data_retorno=array();

		//Llenar la informacion del comprobante
		$documento=array();
		$documento['serie'] = '000';
		$documento['folio'] = 86;
		$documento['fecha'] = date('Y-m-d H:i:s');
		$documento['observaciones'] = '';
		$documento['formapago'] = 99;
		$documento['metodopago'] = 'PPD';
		$documento['descuento'] = 0;
		$documento['regimenfiscal'] = 601;
		$documento['tipodocumento']='I';
		$documento['moneda']= 'MXN';
		$documento['subtotal']=28405.00;
		$documento['total']=28405.00;

		$documento['receptor']=array(
           'rfc' => 'HECF750311V64',
           'nombre' => 'FERNANDO HERNANDEZ CANIZALES',
           'usodelcomprobante' => 'G03',
        );

        $documento['conceptos'] = array(
            0 => array(
                'clave' => '40101701',
                'codigo' => '',
                'cantidad' => 1,
                'codigounidad' => 'H87',
                'unidad' => 'PIEZA',
                'descripcion' => 'PRUEBA',
                'valorUnitario' => '28405.000000',
                'importe' => '28405.000000',
                'descuento' => '0.000000',
                'impuestostrasladados' => array(
                    0 => array(
                        'impuesto' => '002',
                        'base' => '28405',
                        'factor' => 'Tasa',
                        'tasaocuota' => '0',
                        'importe' => '0.000000',
                    )
                )
            )
        );

        $documento['traslados'] = array(
            'totaltraslados' => '0.00',
            'impuestostrasladados' => array(
                0 => array(
                    'impuesto' => '002',
                    'tipofactor' => 'Tasa',
                    'tasaocuota' => '0',
                    'importe' => '0.00',
                )
            )
        );

        //CONSUMO DE SERVICIO
		include('../librerias/nusoap/nusoap.php');

		$oSoapClient = new nusoap_client("http://integratucfdi.com/webservices/wscfdi.php?wsdl", true);
		$err = $oSoapClient->getError();
		if($err!="")
		{
			$data_retorno['error'] ='1'.str_replace("'","\'",$err);
		}
		else
		{
			$respuesta = $oSoapClient->call("GenerarComprobante",
				array (
					'id'=>5090,
					'rfcemisor'=>'AAA010101AAA',
					'idcertificado'=>4171,
					'documento'=>$documento,
					'usuario'=>'usrdemo',
					'password'=>'8b60afbf328296b33204c65cdc149952'
				)
			);
			$err = $oSoapClient->getError();
			if($err!="")
			{
				$data_retorno['error'] ='2'.str_replace("'","\'",$err);
			}
			else
			{
				if($respuesta['resultado']!='')
				{
					$base = str_replace ( 'ejemplos' , '' , getcwd());
					$pathdescargars = $base."descargas/";
					$carpeteexistente=1;
					if (!file_exists($pathdescargars)){
						if(!mkdir($pathdescargars,0777,true)){
							$carpeteexistente=0;
						}
					}

					if($carpeteexistente==1){
						$dir="../descargas/";
						$fileresult=$respuesta['archivos'];
						$strzipresponse=base64_decode($fileresult);
						$filename=$respuesta['uuid'];
						file_put_contents($dir.$filename.'.zip', $strzipresponse);
						file_put_contents($dir.$filename.'.xml', base64_decode($respuesta['xml']));
						file_put_contents($dir.$filename.'.jpg', base64_decode($respuesta['cbb']));

						//Tomar la informacion de Retorno
						$data_retorno['uuid']=$respuesta['uuid'];
						$data_retorno['fechatimbre']=$respuesta['fechatimbre'];
						$data_retorno['sellodigital']=$respuesta['sellodocumento'];
						$data_retorno['seriecertificado']=$respuesta['seriecertificado'];
						$data_retorno['certificadosat']=$respuesta['seriecertificadosat'];
						$data_retorno['sellosat']=$respuesta['sellotimbre'];
						$data_retorno['cadenaoriginal']=$respuesta['cadenaoriginal'];
					}else{
						$data_retorno['error']='No se pudo crear carpeta';
					}
				}else{
					$data_retorno['error']=$respuesta['mensaje'];
				}
			}
		}
		return $data_retorno;
	}

	$data_retorno=timbrarFactura();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Integracion a integratucfdi en php </title>
</head>
<body>
	<a href="index.html" title="Volver">Volver</a>
	<br>
	<?php if(isset($data_retorno['error'])):?>
		<?php echo $data_retorno['error'];?>
	<?php else:?>
		<a href="../descargas/<?php echo $data_retorno['uuid'];?>.xml" download="Xml"  title="Descargar xml">Descarga de archivo xml</a>
		<br>
		<a href="../descargas/<?php echo $data_retorno['uuid'];?>.zip" download="Zip"  title="Descargar zip">Descarga de archivo zip</a>

		<br>
		<table width="100%;">
			<thead>
				<tr>
					<th colspan="2">
						<strong>
							Timbrado factura correctamente
							<br>
							<small>Dato de retorno</small>
						</strong>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data_retorno as $key => $value):?>
					<tr>
						<td>
							<?php echo $key;?>
						</td>
						<td>
							<?php echo $value;?>
						</td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	<?php endif;?>
</body>
</html>

