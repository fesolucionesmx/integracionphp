<?php
	/**
	 * Permite timbrar el recibo de pago de una o varias facturas
	*/
	function timbrarPago()
	{
		$data_retorno=array();

		require_once('../librerias/nusoap/nusoap.php');

		$resp['id'] = 1376921;//idexterno
		$resp['serie'] = '000';
		$resp['folio'] = 86;

		$oSoapClient = new nusoap_client("http://integratucfdi.com/webservices/wscfdi.php?wsdl", true);
		$err = $oSoapClient->getError();

		if($err!=""){
			$resp['msj'] = "Error:".$err;
			return $resp;
		}else{
		 	$oSoapClient->timeout = 300;
			$oSoapClient->response_timeout = 300;

			//--------------------------
				$idsucursal = 5090;
				$empresa_rfc = 'AAA010101AAA';
				$idcertificado = 4171;
				$empresa_usremisor = "usrdemo";
				$empresa_pwdemisor = "8b60afbf328296b33204c65cdc149952";
			//--------------------------

			$regimenfiscal = 601;

			$documento = array();

			$documento["idexterno"] = 1376921;
			$documento["serie"] = '000';
			$documento["folio"] = 86;
			$documento["regimenfiscal"] = 601;

			$rep = array();

			$rep["rfc"] = 'HECF750311V64';
            $rep["nombre"] = 'FERNANDO HERNANDEZ CANIZALES';



			$documento['receptor'] = $rep;

			$rfcbanco = 'XEXX010101000';//RFC del banco de donde se emite el pago
			$nombrebanco = 'BANK OF AMERICA';

			$rfcbancomio = 'BMN930209927';
			$numerocuentamia = '0208833370';

			$pago = array(
				"fechapago" => str_replace(" ","T",date('Y-m-d').' 12:00:00'),
				"formadepago" => 99,
				"moneda" => 'MXN',
				"monto" => 28405.00,
                "numerooperacion" => 1
			);


			$pago['nombrebancoordenante'] = $nombrebanco;



            $pago["rfcentidademisoracuentabeneficiario"] = $rfcbancomio;
            $pago["cuentabeneficiario"] = $numerocuentamia;
            $pago["rfcentidademisoracuentaorigen"] = $rfcbanco;

			$pagos = array(
	            0 => array(
	                    'fechapago' => '2021-04-15T12:00:00',
	                    'formadepago' => '03',
	                    'moneda' => 'MXN',
	                    'tipocambio' => '1',
	                    'monto' => '28405',
	                    'numerooperacion' => '123456',
	                    'rfcentidademisoracuentabeneficiario' => 'BBA940707IE1',
	                    'cuentabeneficiario' => '1265488800',
	                    'cfdis' => array(
	                            0 => array(
	                                    'uuid' => '679ADBFF-7E57-4457-BF9A-030B933E1DC7',
	                                    'serie' => '000',
	                                    'folio' => '86',
	                                    'moneda' => 'MXN',
	                                    'metodopago' => 'PPD',
	                                    'numeroparcialidad' => '1',
	                                    'importesaldoanterior' => '28405',
	                                    'importepagado' => '28405',
	                                    'importesaldoinsoluto' => '0',
	                                )

	                        )
	                )
	        );

			$documento['pagos'] = $pagos;

			$respuesta = $oSoapClient->call("GeneraComprobantePago",array (
													'id' => $idsucursal,
													'rfcemisor' => $empresa_rfc,
													'idcertificado' => $idcertificado,
													'documento' => $documento,
													'usuario' => $empresa_usremisor,
													'password' => $empresa_pwdemisor
												)
											);
		 	if($oSoapClient->fault){
				$data_retorno['error'] = json_encode($respuesta);
		 	}else{
		  		$err = $oSoapClient->getError();
				if ($err){
					$data_retorno['error'] = "Error:".$err;
				}else{
					if($respuesta['resultado']){
						$data_retorno['fechacomprobante'] = $respuesta['fechadocumento'];
						$data_retorno['seriecertificado'] = $respuesta['seriecertificado'];
						$data_retorno['certificadosat'] = $respuesta['seriecertificadosat'];
						$data_retorno['sellosat'] = $respuesta['sellotimbre'];
						$data_retorno['cadenaoriginal'] = $respuesta['cadenaoriginal'];
						$data_retorno['sellodigital'] = $respuesta['sellotimbre'];
						$data_retorno['uuid'] = $respuesta['uuid'];
						$data_retorno['fechatimbre'] = $respuesta['fechatimbre'];

                        //Tomar la informacion de Retorno
                        $dir="../descargas/";
                        //Leer el Archivo Zip
                        $fileresult=$respuesta['archivos'];
                        $strzipresponse=base64_decode($fileresult);
                        $filename=$respuesta['uuid'];
                        file_put_contents($dir.$filename.'.zip', $strzipresponse);
                        file_put_contents($dir.$filename.'.xml', base64_decode($respuesta['xml']));
                        file_put_contents($dir.$filename.'.jpg', base64_decode($respuesta['cbb']));
					}else{
						$data_retorno['error'] = 'Mensaje PAC;'.iconv('ISO-8859-1','UTF-8',$respuesta['mensaje']);

					}
				}
		 	}
		}

		return $data_retorno;
    }
    $data_retorno=timbrarPago();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Integracion a integratucfdi en php </title>
</head>
<body>
	<a href="index.html" title="Volver">Volver</a>
	<br>
	<a href="../descargas/<?php echo $data_retorno['uuid'];?>.xml" download="Xml"  title="Descargar xml">Descarga de archivo xml</a>
	<br>
	<a href="../descargas/<?php echo $data_retorno['uuid'];?>.zip" download="Zip"  title="Descargar zip">Descarga de archivo zip</a>

	<br>
	<table width="100%;">
		<thead>
			<tr>
				<th colspan="2">
					<strong>
						Timbrado pago correctamente
						<br>
						<small>Dato de retorno</small>
					</strong>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data_retorno as $key => $value):?>
				<tr>
					<td>
						<?php echo $key;?>
					</td>
					<td>
						<?php echo $value;?>
					</td>
				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
</body>
</html>

