<?php
	/**
	 * Permite timbrar una nomina
	 */
	function timbrarNomina()
	{
		date_default_timezone_set('America/Mazatlan');

		//Llenamos el arreglo de informacion a enviar para formar el xml
		$documento=array();
		$documento['folio']=5;
		$documento['serie']='N';
		$documento['fecha'] = date("Y-m-d H:i:s");
		$documento['fechapago']='2021-03-15';
		$documento['fechainicial']='2021-03-01';
		$documento['fechafinal']='2021-03-14';
		$documento['diaspagados']=14;
		$documento['periodicidadpago'] = '04';
		$documento['tiponomina']='O';

		//Informacion del empleado
		$documento['empleado']['nombre']='Julio Cesar Alvarado Rios';
		$documento['empleado']['rfc']='AARJ830930QL9';
		$documento['empleado']['curp']='AARJ830930HSLLSL08';
		$documento['empleado']['nss']='23058327059';
		$documento['empleado']['registropatronal']='E4949593102';
		$documento['empleado']['fechacontratacion']='2016-02-16';
		$documento['empleado']['antiguedad']='P264W';
		$documento['empleado']['tipocontrato']='01';
		$documento['empleado']['tipojornada']='01';
		$documento['empleado']['tiporegimen']='02';
		$documento['empleado']['numero']=37;
		$documento['empleado']['departamento']='Sistemas';
		$documento['empleado']['puesto']='Programador';
		$documento['empleado']['claseriesgo'] = 1;
		$documento['empleado']['salariobase']=80;
		$documento['empleado']['sdi']=83.62;
		$documento['empleado']['entidadfederativa']='SIN';
		// $documento['empleado']['banco'] = '002'; //BANAMEX
		// $documento['empleado']['clabe'] = '12345678901234567';

		//Percepciones
		$documento['percepciones']=array();
		/*
			tipo=Codigo SAT de la percepción
			clave=Codigo interno de la percepción
			concepto=Descripcion del concepto de percepción
			importegravado=Importe de la percepción que graba impuestos
			importeexento=Importe de la percepción que no graba impuestos
		*/
		$documento['percepciones'][]=array('tipo'=>'001','clave'=>'SDO','concepto'=>'SUELDO','importegravado'=>1500,'importeexento'=>400);
		$documento['percepciones'][]=array('tipo'=>'010','clave'=>'PPP','concepto'=>'PREMIO POR PUNTUALIDAD','importegravado'=>200,'importeexento'=>50);
		/*
			En caso de tener percepción por horas extras es necesario desglozarlas en simples,dobles y triples
			dias=Numero de dias que trabajo horas extras
			tipo=Tipo de hora extra(01=Simples,02=Dobles,03=Triples)
			horas=Cantidad de horas extras
			importe=importe a percibir por las horas extras
		*/
		$horasextras=array();
		$horasextras[]=array('dias'=>1,'tipo' => '01','horas' => 3, 'importe' => 60);
		$horasextras[]=array('dias' => 2,'tipo' => '02','horas' => 2,'importe' => 80);
		$horasextras[]=array('dias' => 1,'tipo' => '03', 'horas' => 1,'importe' => 60);
		//Agregamos la percepciones de las horas extras
		$documento['percepciones'][]=array(
			'tipo'=>'019',
			'clave'=>'T EXT',
			'concepto'=>'TIEMPO EXTRA',
			'importegravado'=>150,
			'importeexento'=>50,
			'horasextras' => $horasextras
		);

		//Deducciones
		$documento['deducciones']=array();
		/*
			tipo=Codigo SAT de la deducción
			clave=Codigo interno de la deducción
			concepto=Descripcion del concepto
			importe=Importe de la deducción
		*/
		$documento['deducciones'][]=array('tipo'=>'002','clave'=>'ISPT','concepto'=>'ISPT','importe'=>250);
		$documento['deducciones'][]=array('tipo'=>'001','clave'=>'IMSS','concepto'=>'IMSS','importe'=>100);

		//Otros Pagos (el subsidio al empledo debe ir en otros pagos aun cuando sea cero el importe arecibir por este concepto)
		$documento['otrospagos']=array();
		/*
			tipo=Codigo SAT de la deducción
			clave=Codigo interno de la percepcion otro pago
			concepto=Descripcion de la percepcion otro pago
			importe=Importe de la percepcion otro pago
			subsidiocausado=importe del subsidio causado (este campo se envía solo en caso de ser el subsidio para el empleo codigo SAT 002)
		*/
		$documento['otrospagos'][]=array('tipo'=>'002','clave'=>'SEMPL','concepto'=>'SUBSIDIO AL EMPLEO','importe'=>0,'subsidiocausado'=>250);

		//Mandar Timbrar el comprobante
		require_once('../librerias/nusoap/nusoap.php');
		$oSoapClient = new nusoap_client("http://integratucfdi.com/webservices/wscfdi.php?wsdl", true);

		$err = $oSoapClient->getError();
		if($err!="")
			$data_retorno['error']= "error:".$err;
		else
		{
			//Tomar los datos de coneccion
			$respuestas = $oSoapClient->call("GenerarRecibosNomina", array(
												'id' => 1,
												'rfcemisor' => 'HECF750311V64',
												'idcertificado' => 1,
												'recibos' => array($documento),
												'usuario' => 'pruebas',
												'password' => 'ee2ec3cc66427bb422894495068222a8'
											)
			);

			if ($oSoapClient->fault)
			{
				$data_retorno['error']= 'ERROR de conexión con servidor de timbrado';
			}
			else
			{
				$err = $oSoapClient->getError();
				if ($err)
				{
					$data_retorno['error']= 'Error:'.$err;
				}
				else
				{
					foreach($respuestas as $respuesta)
					{
						if($respuesta['resultado'])
						{
							$data_retorno['fecha']=$respuesta['fechadocumento'];
							$data_retorno['seriecertificado']=$respuesta['seriecertificado'];
							$data_retorno['certificadosat']=$respuesta['seriecertificadosat'];
							$data_retorno['sellosat']=$respuesta['sellotimbre'];
							$data_retorno['cadenatimbre']=$respuesta['cadenaoriginal'];
							$data_retorno['sellodigital']=$respuesta['sellodocumento'];
							$data_retorno['uuid']=$respuesta['uuid'];
							$data_retorno['fechatimbre']=$respuesta['fechatimbre'];

							//Guardar los archivos en disco
							$strzipresponse = base64_decode($respuesta['archivos']);
							$dir='../descargas/';
							$filename = $respuesta['uuid'];
							file_put_contents($dir.$filename.'.zip', $strzipresponse);
							$zip = new ZipArchive;
							if ($zip->open($dir.$filename.'.zip') === TRUE)
							{
								file_put_contents($dir.$filename.'.xml', base64_decode($respuesta['xml']));
								$strcbb=$zip->getFromName('cbb.jpg');
								file_put_contents($dir.$filename.'.jpg', $strcbb);
								// $strxml=$zip->getFromName('xml.xml');
								$zip->close();
							}
							else{
								$data_retorno['error']='Error al descomprimir el archivo';
							}
						}
						else
						{
							$data_retorno['error']= 'Error'.iconv("ISO-8859-1", "UTF-8", $respuesta['mensaje']);
						}
					}
				}
			}
		}
		return $data_retorno;
	}

	$data_retorno=timbrarNomina();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Integracion a integratucfdi en php </title>
</head>
<body>
	<a href="index.html" title="Volver">Volver</a>

	<?php if(isset($data_retorno['error'])):?>
		<?php echo $data_retorno['error'];?>
	<?php else:?>
		<br>
		<a href="../descargas/<?php echo $data_retorno['uuid'];?>.xml" download="Xml"  title="Descargar xml">Descarga de archivo xml</a>
		<br>
		<a href="../descargas/<?php echo $data_retorno['uuid'];?>.zip" download="Zip"  title="Descargar zip">Descarga de archivo zip</a>
		<br>
		<table width="100%;">
			<thead>
				<tr>
					<th colspan="2">
						<strong>
							Timbrado nómina correctamente
							<br>
							<small>Dato de retorno</small>
						</strong>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data_retorno as $key => $value):?>
					<tr>
						<td>
							<?php echo $key;?>
						</td>
						<td>
							<?php echo $value;?>
						</td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	<?php endif;?>
</body>
</html>

