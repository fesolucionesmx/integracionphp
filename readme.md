# Test de ayuda para consumir servicios de integratucfdi.com en lenguaje php

integracionphp

---------------------------------------------------------------------

### Requerimientos

- PHP5
- nusoap v 1.123

### Uso

En los archivos de la carpeta ejemplo se encuentra la definición de las funciones que hacen consumo de los servicios de integratucfdi.com para timbrado y cancelación de cfdi 3.3.

**Importante**: Los datos utilizados en los ejemplos, son datos de prueba. No deben usarse para casos reales.

## Desarrollado por

- fesoluciones
